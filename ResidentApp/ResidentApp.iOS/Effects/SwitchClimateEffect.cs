﻿using System;
using System.Linq;
using ResidentApp.CustomControls.Effects;
using ResidentApp.iOS.Effects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("Resident")]
[assembly: ExportEffect(typeof(SwitchClimateEffect), "SwitchClimateEffect")]
namespace ResidentApp.iOS.Effects
{
    public class SwitchClimateEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                var control = Control as UISwitch;
                var effect = (SwitchClimateModeEffect)Element.Effects.FirstOrDefault(e => e is SwitchClimateModeEffect);
                if (effect != null && control != null)
                {
                    control.OnTintColor = effect.ColorOn.ToUIColor();
                    control.TintColor = effect.ColorOff.ToUIColor();
                    control.BackgroundColor = effect.ColorOff.ToUIColor();
                    control.Layer.CornerRadius = control.Frame.Height / 2;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot set property on attached control. Error: {ex.Message}");
            }
        }

        protected override void OnDetached()
        {
           
        }
    }
}