﻿using System;
using Foundation;
using ResidentApp.iOS.Effects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(UnderlineEffect), "UnderlineEffect")]
namespace ResidentApp.iOS.Effects
{
    public class UnderlineEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                if (Control is UILabel control)
                {
                    control.AttributedText = new NSAttributedString(control.Text, underlineStyle: NSUnderlineStyle.Single);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot set property on attached control. Error: {ex.Message}");
            }
        }

        protected override void OnDetached()
        {

        }
    }
}