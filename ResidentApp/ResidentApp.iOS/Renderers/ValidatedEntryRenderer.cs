﻿using System;
using ResidentApp.CustomControls;
using ResidentApp.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ValidatedEntry), typeof(ValidatedEntryRenderer))]
namespace ResidentApp.iOS.Renderers
{
    public class ValidatedEntryRenderer : EntryRenderer
    {
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null || this.Element == null) return;

            if (e.PropertyName == ValidatedEntry.IsBorderErrorVisibleProperty.PropertyName &&
                Element is ValidatedEntry currentEntry)
            {
                if (currentEntry.IsBorderErrorVisible)
                {
                    this.Control.TextColor = currentEntry.BorderErrorColor.ToUIColor();
                    this.Control.Layer.BorderColor = currentEntry.BorderErrorColor.ToCGColor();
                    this.Control.Layer.BorderWidth = new nfloat(0.8);
                    this.Control.Layer.CornerRadius = 5;
                }
                else
                {
                    this.Control.TextColor = currentEntry.TextColor.ToUIColor();
                    this.Control.Layer.BorderColor = UIColor.LightGray.CGColor;
                    this.Control.Layer.CornerRadius = 5;
                    this.Control.Layer.BorderWidth = new nfloat(0.8);
                }
            }
        }

    }
}