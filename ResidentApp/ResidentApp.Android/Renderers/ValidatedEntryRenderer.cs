﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using ResidentApp.CustomControls;
using ResidentApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ValidatedEntry), typeof(ValidatedEntryRenderer))]
namespace ResidentApp.Droid.Renderers
{
    public class ValidatedEntryRenderer : EntryRenderer
    {
        public ValidatedEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null || e.NewElement == null) return;

            UpdateBorders();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null) return;

            if (e.PropertyName == ValidatedEntry.IsBorderErrorVisibleProperty.PropertyName)
                UpdateBorders();
        }

        private void UpdateBorders()
        {
            GradientDrawable shape = new GradientDrawable();
            shape.SetShape(ShapeType.Rectangle);
            shape.SetCornerRadius(0);
            if (Element is ValidatedEntry currentEntry)
            {
                if (currentEntry.IsBorderErrorVisible)
                {
                    shape.SetStroke(3, currentEntry.BorderErrorColor.ToAndroid());
                    Control.SetTextColor(currentEntry.BorderErrorColor.ToAndroid());
                }
                else
                {
                    shape.SetStroke(3, Android.Graphics.Color.LightGray);
                    Control.SetTextColor(currentEntry.TextColor.ToAndroid());
                    Control.SetBackground(shape);
                }
            }
            Control.SetBackground(shape);
        }

    }
}