﻿using System;
using System.Linq;
using Android.Graphics.Drawables;
using Android.OS;
using ResidentApp.CustomControls.Effects;
using ResidentApp.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("Resident")]
[assembly: ExportEffect(typeof(SwitchClimateEffect), "SwitchClimateEffect")]
namespace ResidentApp.Droid.Effects
{
    public class SwitchClimateEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                var control = Control as Android.Widget.Switch;
                var effect = (SwitchClimateModeEffect)Element.Effects.FirstOrDefault(e => e is SwitchClimateModeEffect);
                if (effect != null && control != null)
                {
                    Android.Graphics.Color colorOn = effect.ColorOn.ToAndroid();
                    Android.Graphics.Color colorOff = effect.ColorOff.ToAndroid();
                    StateListDrawable drawable = new StateListDrawable();
                    drawable.AddState(new int[] {Android.Resource.Attribute.StateChecked}, new ColorDrawable(colorOn));
                    drawable.AddState(new int[] { }, new ColorDrawable(colorOff));
                    control.ThumbDrawable = drawable;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot set property on attached control. Error: {ex.Message}");
            }
        }

        protected override void OnDetached()
        {
            
        }
    }
}