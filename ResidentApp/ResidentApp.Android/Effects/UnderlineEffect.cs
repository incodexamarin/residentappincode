﻿using System;
using Android.Graphics;
using Android.Widget;
using ResidentApp.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportEffect(typeof(UnderlineEffect), "UnderlineEffect")]

namespace ResidentApp.Droid.Effects
{
    public class UnderlineEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                if (Control is TextView control)
                {
                    control.PaintFlags = PaintFlags.UnderlineText;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot set property on attached control. Error: {ex.Message}");
            }
        }

        protected override void OnDetached()
        {
           
        }
    }
}