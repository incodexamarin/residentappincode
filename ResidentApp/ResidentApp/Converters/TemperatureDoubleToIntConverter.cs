﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace ResidentApp.Converters
{
    public class TemperatureDoubleToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return System.Convert.ToDouble((int)value);
            }
            catch (Exception e)
            {
                Console.WriteLine("-----------------------------------Double to int converter error: " + e.Message);
                return default(double);
            }
           
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return System.Convert.ToInt32((double)value);
            }
            catch (Exception e)
            {
                Console.WriteLine("-----------------------------------Double to int converter error: " + e.Message);
                return default(int);
            }
        }
    }
}