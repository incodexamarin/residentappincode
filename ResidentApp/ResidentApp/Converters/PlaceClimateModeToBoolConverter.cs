﻿using System;
using System.Globalization;
using HabSDK.Model;
using Xamarin.Forms;

namespace ResidentApp.Converters
{
    public class PlaceClimateModeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                PlaceResource.ClimateModeEnum climateMode = (PlaceResource.ClimateModeEnum)value;
                return climateMode == PlaceResource.ClimateModeEnum.Heat;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(PlaceResource.ClimateModeEnum);
            }
           
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                bool climateFlag = (bool) value;
                return climateFlag ? PlaceResource.ClimateModeEnum.Heat : PlaceResource.ClimateModeEnum.Cool;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(bool);
            }
        }
        
    }
}