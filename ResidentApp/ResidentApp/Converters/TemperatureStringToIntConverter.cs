﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace ResidentApp.Converters
{
    public class TemperatureStringToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return value.ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine("-----------------------------------String to int converter error: " + e.Message);
                return default(string);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return value is string temperature && Int32.TryParse(temperature, out int target) ? target : default(int);
            }
            catch (Exception e)
            {
                Console.WriteLine("-----------------------------------String to int converter error: " + e.Message);
                return default(int);
            }
        }
    }
}