﻿using System;
using System.Globalization;
using HabSDK.Model;
using ResidentApp.ViewModels.PageModels;
using Xamarin.Forms;

namespace ResidentApp.Converters
{
    public class TemperatureBoolToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return (bool)value ? Color.LightCoral : Color.Black;
            }
            catch (Exception e)
            {
                Console.WriteLine("-----------------------------------Bool to color converter error: "+e.Message);
                return Color.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return default(bool);
            }
            catch (Exception e)
            {
                Console.WriteLine("-----------------------------------Bool to color converter error: " + e.Message);
                return default(bool);
            }
        }
    }
}