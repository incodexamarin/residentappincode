﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using ResidentApp.Models;
using ResidentApp.Services.Abstractions;
using ResidentApp.Utils;
using ResidentApp.Views;
using Xamarin.Forms;

namespace ResidentApp.Services
{
    public class AccessTokenApiService : AccessTokenApi, IAccessTokenApiService
    {
        private readonly IAccessTokenApi _aceAccessTokenApi;

        public AccessTokenApiService()
        {
            _aceAccessTokenApi = new AccessTokenApi();
        }

        public async Task ValidateTokenAsync()
        {
            try
            {
                HabSDK.Client.Configuration.Default.AddDefaultHeader(Constants.APP_KEY,
                    Settings.Current.Get("AppName", "thermo-dev"));
                var oauth = Settings.Current.Get<OAuthEntity>("OAuthToken");
                if ((DateTime.UtcNow - oauth.CreationDate).TotalSeconds > Convert.ToInt32(oauth.OAuth.ExpiresIn))
                {
                    Debug.WriteLine($"Old OAuth:" + Environment.NewLine + oauth.OAuth);
                    var result = await _aceAccessTokenApi.GetOAuthTokenAsync(
                        Constants.GrandTypes.refresh_token.ToString(), Constants.ClIENT_ID, "", "", "", "",
                        oauth.OAuth.RefreshToken);
                    Debug.WriteLine($"New OAuth:" + Environment.NewLine + result);
                    OAuthEntity response = new OAuthEntity(result, DateTime.UtcNow);
                    Settings.Current.Set("OAuthToken", response);
                    com.knetikcloud.Client.Configuration.Default.AccessToken = response.OAuth.AccessToken;
                    HabSDK.Client.Configuration.Default.AccessToken = response.OAuth.AccessToken;
                }
                else
                {
                    com.knetikcloud.Client.Configuration.Default.AccessToken = oauth.OAuth.AccessToken;
                    HabSDK.Client.Configuration.Default.AccessToken = oauth.OAuth.AccessToken;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message, TimeSpan.FromSeconds(2));
                Application.Current.MainPage = new NavigationPage(new LoginPage());
            }
        }
    }
}