﻿using System.Threading.Tasks;
using com.knetikcloud.Api;

namespace ResidentApp.Services.Abstractions
{
    public interface IAccessTokenApiService : IAccessTokenApi
    {
        Task ValidateTokenAsync();
    }
}