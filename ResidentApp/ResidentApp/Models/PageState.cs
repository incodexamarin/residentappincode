﻿namespace ResidentApp.Models
{
    public enum PageState
    {
        Normal,
        Loading,
        NoData
    }
}