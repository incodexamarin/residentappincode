﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResidentApp.Utils
{
    public class Constants
    {
        public static string SECRET_KEY = "";
        public static string CLIENT_KEY = "knetik";
        public static string ClIENT_ID = "knetik";

        public static string KNETIK_JSPAI_URL = "https://thermo-dev.devsandbox.knetikcloud.com";
        public static string KNETIK_NOTIFICATION_URL = "https://thermo-dev.devsandbox.knetikcloud.com";
        
        public static string HAB_API_LINK = "https://hab-api.devsandbox.knetikcloud.com";

        public static string APP_KEY = "x-knetikcloud-appid";

        //Regular expressions
        public static string EmailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                                          @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        public enum GrandTypes
        {
            client_credentials,
            password,
            facebook,
            google,
            refresh_token
        }

        public enum AppKeys
        {
            Email,
            password,
            appName
        }
    }
}
