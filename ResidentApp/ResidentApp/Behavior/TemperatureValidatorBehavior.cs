﻿using System;
using Acr.UserDialogs;
using HabSDK.Model;
using ResidentApp.CustomControls;
using ResidentApp.ViewModels.PageModels;
using Xamarin.Forms;
using Color = System.Drawing.Color;

namespace ResidentApp.Behavior
{
    public class TemperatureValidatorBehavior : Behavior<ValidatedEntry>
    {
        private ValidatedEntry _control;
       

        protected override void OnAttachedTo(ValidatedEntry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
            //bindable.PropertyChanged += OnPropertyChanged;
            _control = bindable;
        }

        protected void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            
            if (!string.IsNullOrEmpty(e.NewTextValue) && sender is ValidatedEntry currentEntry && currentEntry.IsBorderErrorVisible)
            {
                var toastConfig = new ToastConfig(_control.ErrorText);

                //toastConfig.SetBackgroundColor(Color.Crimson);
                UserDialogs.Instance.Toast(toastConfig);
            }
            
            //if (!string.IsNullOrEmpty(e.NewTextValue) && currentEntry?.PlaceInstance != null && Decimal.TryParse(currentEntry.Text, out decimal temperature))
            //{
            //    bool coolLimit = (currentEntry.PlaceInstance.ClimateMode == PlaceResource.ClimateModeEnum.Heat &&
            //                      (currentEntry.PlaceInstance.HeatMax > temperature && temperature > currentEntry.PlaceInstance.HeatMin));
            //    bool heatLimit = ((currentEntry.PlaceInstance.ClimateMode == PlaceResource.ClimateModeEnum.Cool &&
            //                       (currentEntry.PlaceInstance.CoolMax > temperature && temperature > currentEntry.PlaceInstance.CoolMin)));
            //    currentEntry.IsBorderErrorVisible = coolLimit | heatLimit;
            //}
        }

        protected void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ValidatedEntry.IsBorderErrorVisibleProperty.PropertyName && _control != null)
            {
                if (_control.IsBorderErrorVisible)
                {
                    
                    //_control.Text = string.Empty;
                }
            }
        }

        protected override void OnDetachingFrom(ValidatedEntry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
            //bindable.PropertyChanged -= OnPropertyChanged;
        }
    }
}