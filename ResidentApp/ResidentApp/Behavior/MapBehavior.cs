﻿using System;
using ResidentApp.CustomControls;
using ResidentApp.Utils;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace ResidentApp.Behavior
{
    public class MapBehavior : BindableBehavior<Map>
    {
        protected override void OnAttachedTo(Map bindable)
        {
            base.OnAttachedTo(bindable);
            //AssociatedObject.PropertyChanged += OnPropertyChanged;
            AssociatedObject.MoveToRegion(new MapSpan(new Position(0, 0), 0, 0).WithZoom(0));
        }

        protected void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CustomMap.ShownPositionProperty.PropertyName)
            {
                //AssociatedObject?.MoveToRegion(MapSpan.FromCenterAndRadius(AssociatedObject.ShownPosition, Distance.FromMiles(9999)));
            }
        }

        protected override void OnDetachingFrom(Map bindable)
        {
            base.OnDetachingFrom(bindable);
            //AssociatedObject.PropertyChanged -= OnPropertyChanged;
        }
    }
}