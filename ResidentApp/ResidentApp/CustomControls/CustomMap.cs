﻿using ResidentApp.Behavior;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace ResidentApp.CustomControls
{
    public class CustomMap : Map
    {
        public static readonly BindableProperty ShownPositionProperty =
            BindableProperty.Create(nameof(ShownPosition), typeof(Position), typeof(CustomMap), new Position());

        public Position ShownPosition
        {
            get { return (Position)GetValue(ShownPositionProperty); }
            set { SetValue(ShownPositionProperty, value); }
        }
    }
}