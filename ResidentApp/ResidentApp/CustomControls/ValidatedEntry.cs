﻿using ResidentApp.ViewModels.PageModels;
using Xamarin.Forms;

namespace ResidentApp.CustomControls
{
    public class ValidatedEntry : Entry
    {
        public static readonly BindableProperty IsBorderErrorVisibleProperty =
            BindableProperty.Create(nameof(IsBorderErrorVisible), typeof(bool), typeof(ValidatedEntry), false, BindingMode.TwoWay);

        public bool IsBorderErrorVisible
        {
            get { return (bool)GetValue(IsBorderErrorVisibleProperty); }
            set { SetValue(IsBorderErrorVisibleProperty, value); }
        }

        public static readonly BindableProperty BorderErrorColorProperty =
            BindableProperty.Create(nameof(BorderErrorColor), typeof(Color), typeof(ValidatedEntry), Color.Transparent, BindingMode.TwoWay);

        public Color BorderErrorColor
        {
            get { return (Color)GetValue(BorderErrorColorProperty); }
            set { SetValue(BorderErrorColorProperty, value); }
        }

        public static readonly BindableProperty ErrorTextProperty =
            BindableProperty.Create(nameof(ErrorText), typeof(string), typeof(ValidatedEntry), string.Empty);

        public string ErrorText
        {
            get { return (string)GetValue(ErrorTextProperty); }
            set { SetValue(ErrorTextProperty, value); }
        }
    }
}