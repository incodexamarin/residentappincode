﻿using Xamarin.Forms;

namespace ResidentApp.CustomControls.Effects
{
    public class SwitchClimateModeEffect : RoutingEffect
    {
        public Color ColorOn { get; set; }
        public Color ColorOff { get; set; }
        public SwitchClimateModeEffect() : base("Resident.SwitchClimateEffect")
        {
        }
    }
}