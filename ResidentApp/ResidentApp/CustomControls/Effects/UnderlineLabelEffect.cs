﻿using Xamarin.Forms;

namespace ResidentApp.CustomControls.Effects
{
    public class UnderlineLabelEffect : RoutingEffect
    {
        public UnderlineLabelEffect() : base("Resident.UnderlineEffect")
        {
        }
    }
}