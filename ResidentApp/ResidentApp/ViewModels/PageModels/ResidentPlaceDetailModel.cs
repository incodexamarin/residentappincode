﻿using System;
using HabSDK.Model;
using Prism.Mvvm;

namespace ResidentApp.ViewModels.PageModels
{
    public class ResidentPlaceDetailModel : BindableBase
    {
        public ResidentPlaceDetailModel(string name = default(string))
        {
            Name = name;
        }
        #region Properties
        private string _id;

        public string Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private int? _targetTemp;

        public int? TargetTemp
        {
            get { return _targetTemp; }
            set { SetProperty(ref _targetTemp, value); }
        }

        private decimal? _currentTemp;

        public decimal? CurrentTemp
        {
            get { return _currentTemp; }
            set { SetProperty(ref _currentTemp, value); }
        }

        private decimal? _coolMax;

        public decimal? CoolMax
        {
            get { return _coolMax; }
            set { SetProperty(ref _coolMax, value); }
        }

        private decimal? _coolMin;

        public decimal? CoolMin
        {
            get { return _coolMin; }
            set { SetProperty(ref _coolMin, value); }
        }

        private decimal? _heatMax;

        public decimal? HeatMax
        {
            get { return _heatMax; }
            set { SetProperty(ref _heatMax, value); }
        }

        private decimal? _heatMin;

        public decimal? HeatMin
        {
            get { return _heatMin; }
            set { SetProperty(ref _heatMin, value); }
        }

        private decimal? _shownMin;

        public decimal? ShownMin
        {
            get { return _shownMin; }
            set { SetProperty(ref _shownMin, value); }
        }

        private decimal? _shownMax;

        public decimal? ShownMax
        {
            get { return _shownMax; }
            set { SetProperty(ref _shownMax, value); }
        }

        private PlaceResource.ClimateModeEnum? _climateMode;

        public PlaceResource.ClimateModeEnum? ClimateMode
        {
            get { return _climateMode; }
            set
            {
                SetProperty(ref _climateMode, value);
                ShownMax = _climateMode == PlaceResource.ClimateModeEnum.Heat ? HeatMax : CoolMax;
                ShownMin = _climateMode == PlaceResource.ClimateModeEnum.Heat ? HeatMin : CoolMin;
            }
        }

        #endregion
    }
}