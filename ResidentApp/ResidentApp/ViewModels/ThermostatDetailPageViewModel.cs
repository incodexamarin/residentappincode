﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using HabSDK.Api;
using HabSDK.Client;
using HabSDK.Model;
using Prism.Navigation;
using Prism.Services;
using ResidentApp.Models;
using ResidentApp.Services.Abstractions;
using ResidentApp.ViewModels.PageModels;

namespace ResidentApp.ViewModels
{
    public class ThermostatDetailPageViewModel : ViewModelBase
    {
        private DelegateCommand _mapCommand;
        public DelegateCommand MapCommand => _mapCommand ?? (_mapCommand =
                                                        new DelegateCommand(OnMapCommandExecuted));

        private DelegateCommand _savePlaceCommand;

        public DelegateCommand SavePlaceCommand => _savePlaceCommand ?? (_savePlaceCommand =
                                                       new DelegateCommand(OnSavePlaceCommandExecuted,
                                                           SavePlaceCommandCanExecute)
                                                           .ObservesProperty(() => Place)
                                                           .ObservesProperty(() => Place.ClimateMode)
                                                           .ObservesProperty(() => Place.CoolMax)
                                                           .ObservesProperty(() => Place.CoolMin)
                                                           .ObservesProperty(() => Place.HeatMax)
                                                           .ObservesProperty(() => Place.HeatMin)
                                                           .ObservesProperty(() => Place.TargetTemp));
                                                       

        private IResidentsApi _residentsApi;
        private readonly IPageDialogService _pageDialogService;

        public ThermostatDetailPageViewModel(INavigationService navigationService,
            IAccessTokenApiService accessTokenApiService, IResidentsApi residentsApi,
            IPageDialogService pageDialogService) : base(navigationService, accessTokenApiService)
        {
            _residentsApi = residentsApi;
            _pageDialogService = pageDialogService;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _residentsApi.Configuration.AccessToken = accessToken;
        }

        private PageState _state = PageState.NoData;
        public PageState State
        {
            get => _state;
            set => SetProperty(ref _state, value);
        }

        private ResidentPlaceDetailModel _place;
        public ResidentPlaceDetailModel Place
        {
            get { return _place; }
            set { SetProperty(ref _place, value); }
        }

        private bool _isNotValid;
        public bool IsNotValid
        {
            get { return _isNotValid; }
            set { SetProperty(ref _isNotValid, value); }
        }
        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            try
            {
                State = PageState.Loading;
                PageResourcePlaceResource places = await _residentsApi.GetResidentPlacesAsync();
                //if (parameters.ContainsKey("places"))
                if (places?.Content.Count > 0)
                {
                    //places = (PageResourcePlaceResource)parameters["places"];
                    PlaceResource place = places?.Content.Count > 0 ? places.Content[0] : null;

                    if (place != null)
                    {
                        Place = new ResidentPlaceDetailModel(place.Name)
                        {
                            Id = place.Id.ToString(),
                            CoolMax = place.CoolMax,
                            CoolMin = place.CoolMin,
                            HeatMax = place.HeatMax,
                            HeatMin = place.HeatMin,
                            ClimateMode = place.ClimateMode,
                            TargetTemp = Convert.ToInt32(place.CurrentTemp),
                            CurrentTemp = place.CurrentTemp
                        };
                        State = PageState.Normal;
                    }
                    else
                    {
                        State = PageState.NoData;
                        Place = new ResidentPlaceDetailModel("New Place");
                    }
                }
            }
            catch (Exception exception)
            {
                await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
            }
        }
     


        private async void OnMapCommandExecuted()
        {
            await _navigationService.NavigateAsync("Map");
        }

        private async void OnSavePlaceCommandExecuted()
        {
            var resultDialog = await _pageDialogService.DisplayAlertAsync("Updating place","Do you want to update this place?", "Yes", "No");
            //var resultDialog = await _pageDialogService.DisplayActionSheetAsync("Do you want to update this place?", "No", "Yes");
            if (!resultDialog) return;;

            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");

                await CheckAccessToken();
                var updatedResidentPlace = new UpdateResidentPlaceResource();
                updatedResidentPlace.ClimateMode = (UpdateResidentPlaceResource.ClimateModeEnum?)Place.ClimateMode;
                switch (Place.ClimateMode)
                {
                    case PlaceResource.ClimateModeEnum.Cool:
                        updatedResidentPlace.CoolTarget = Place.TargetTemp;
                        break;
                    case PlaceResource.ClimateModeEnum.Heat:
                        updatedResidentPlace.HeatTarget = Place.TargetTemp;
                        break;
                }
                Debug.WriteLine(updatedResidentPlace.ToJson());
                await _residentsApi.UpdateResidentPlaceAsync(Place.Id, updatedResidentPlace);
                UserDialogs.Instance.Toast("Resident place has been updated successfully.", TimeSpan.FromSeconds(2));
            }
            catch (Exception exception)
            {
                await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private bool SavePlaceCommandCanExecute()
        {
            if (Place == null) return false;
            bool coolLimit = (Place.ClimateMode == PlaceResource.ClimateModeEnum.Heat &&
                              (Place.HeatMax > Place.TargetTemp && Place.TargetTemp > Place.HeatMin));
            bool heatLimit = ((Place.ClimateMode == PlaceResource.ClimateModeEnum.Cool &&
                               (Place.CoolMax > Place.TargetTemp && Place.TargetTemp > Place.CoolMin)));
            IsNotValid = !(coolLimit | heatLimit);
            return (coolLimit | heatLimit);
        }
    }
}
