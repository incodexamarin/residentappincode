﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Acr.UserDialogs;
using Prism.Navigation;
using Prism.Services;
using ResidentApp.Services.Abstractions;

namespace ResidentApp.ViewModels
{
	public class MasterPageViewModel : ViewModelBase
    {
        private DelegateCommand<string> _navigateCommand;
        public DelegateCommand<string> NavigateCommand => _navigateCommand ?? (_navigateCommand =
                                                 new DelegateCommand<string>(OnNavigateCommandExecuted));

        private DelegateCommand _logoutCommand;
        public DelegateCommand LogoutCommand => _logoutCommand ?? (_logoutCommand =
                                                    new DelegateCommand(OnLogoutCommandExecuted));

        private readonly IPageDialogService _pageDialogService;
        public MasterPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService, IPageDialogService pageDialogService) : base(navigationService, accessTokenApiService)
        {
            _pageDialogService = pageDialogService;
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            try
            {
                if (parameters.ContainsKey("places"))
                {
                    //TODO: Implement data handling
                }
            }
            catch (Exception exception)
            {
                await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
            }

        }

        private void OnNavigateCommandExecuted(string reference)
        {
            try
            {
               _navigationService.NavigateAsync($"Navigation/{reference}");
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(2));
            }
        }

        private async void OnLogoutCommandExecuted()
        {
            try
            {
                //if (Settings.Current.Remove("CurrentUser") && Settings.Current.Remove("OAuthToken"))
                //    Application.Current.MainPage = new NavigationPage(new LoginPage());
                await _navigationService.GoBackAsync();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Toast(ex.Message, TimeSpan.FromSeconds(2));
            }
        }
    }
}
