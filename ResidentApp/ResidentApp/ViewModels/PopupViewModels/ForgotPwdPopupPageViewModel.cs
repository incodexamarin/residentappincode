﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Model;
using HabSDK.Api;
using HabSDK.Client;
using Prism.Navigation;
using Prism.Services;
using ResidentApp.Services.Abstractions;
using ResidentApp.Utils;
using IUsersApi = com.knetikcloud.Api.IUsersApi;

namespace ResidentApp.ViewModels.PopupViewModels
{
    public class ForgotPwdPopupPageViewModel : ViewModelBase
    {
        private DelegateCommand _sendCommand;

        public DelegateCommand SendCommand => _sendCommand ?? (_sendCommand =
                                                  new DelegateCommand(OnSendCommandExecuted, SendCommandCanExecute)
                                                      .ObservesProperty(() => Email));

        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersApi _usersApi;

        public ForgotPwdPopupPageViewModel(INavigationService navigationService,
            IAccessTokenApiService accessTokenApiService, IUsersApi usersApi, IPageDialogService pageDialogService) :
            base(navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;
            _pageDialogService = pageDialogService;
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private async void OnSendCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                await _usersApi.SubmitPasswordResetAsync(new PasswordResetRequest(Email = Email));
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
                await _navigationService.GoBackAsync();
            }
        }

        private bool SendCommandCanExecute() => !string.IsNullOrWhiteSpace(Email) &&
                                                Regex.IsMatch(Email, Constants.EmailRegex, RegexOptions.IgnoreCase) &&
                                                IsNotBusy;
    }
}
