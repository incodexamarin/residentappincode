﻿using Prism.Commands;
using System;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using HabSDK.Api;
using HabSDK.Model;
using Prism.Navigation;
using Prism.Services;
using ResidentApp.Models;
using ResidentApp.Services.Abstractions;
using ResidentApp.Utils;
using ApiException = HabSDK.Client.ApiException;

namespace ResidentApp.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IResidentsApi _residentsApi;

        public LoginPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService,
            IPageDialogService pageDialogService, IResidentsApi residentsApi) : base(
            navigationService, accessTokenApiService)
        {
            _residentsApi = residentsApi;
            _pageDialogService = pageDialogService;
            Email = "3@3.com";
            Password = "mocko-mocko";
            AppName = "thermo-dev";
        }

        private DelegateCommand _loginCommand;

        public DelegateCommand LoginCommand => _loginCommand ?? (_loginCommand =
                                                   new DelegateCommand(OnLoginCommandExecuted, LoginCommandCanExecute)
                                                       .ObservesProperty(() => Email)
                                                       .ObservesProperty(() => Password));

        private DelegateCommand _forgotPasswordCommand;

        public DelegateCommand ForgotPasswordCommand => _forgotPasswordCommand ?? (_forgotPasswordCommand =
                                                            new DelegateCommand(OnForgotPasswordCommandExecuted));

        private async void OnLoginCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");

                OAuth2Resource result = await _accessTokenApiService.GetOAuthTokenAsync(
                    Constants.GrandTypes.password.ToString(), Constants.ClIENT_ID, "", Email, Password, "", "");
                OAuthEntity response = new OAuthEntity(result, DateTime.UtcNow);
                Settings.Current.Set("OAuthToken", response);
                Settings.Current.Set("AppName", AppName); //AppName = thermo-dev

                Configuration.Default.AccessToken = result.AccessToken;
                HabSDK.Client.Configuration.Default.AccessToken = result.AccessToken;
                HabSDK.Client.Configuration.Default.AddDefaultHeader(Constants.APP_KEY, AppName);

                //PageResourcePlaceResource places = await _residentsApi.GetResidentPlacesAsync();
                //if (places?.Content.Count == 0)
                //{
                //    UserDialogs.Instance.Toast("No Device Available");
                //}
                //else if (places != null)
                //{
                //    var paramerter = new NavigationParameters { { "places", places } };
                //    await _navigationService.NavigateAsync("Index/Navigation/Thermostat", paramerter);
                //}

                await _navigationService.NavigateAsync("Index/Navigation/Thermostat");
            }
            catch (ApiException ex)
            {
                UserDialogs.Instance.Toast(ex.ErrorContent, TimeSpan.FromSeconds(2));
            }
            catch (Exception exception)
            {
                await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private string _appName;
        public string AppName
        {
            get { return _appName; }
            set { SetProperty(ref _appName, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private async void OnForgotPasswordCommandExecuted()
        {
            await _navigationService.NavigateAsync("ForgotPassword");
        }

        private bool LoginCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(Email) &&
            !string.IsNullOrWhiteSpace(Password); //&& Regex.IsMatch(Email, Constants.EmailRegex, RegexOptions.IgnoreCase) && IsNotBusy;

    }
}
