﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using HabSDK.Model;
using Prism.Navigation;
using ResidentApp.Models;
using ResidentApp.Services.Abstractions;

namespace ResidentApp.ViewModels
{
    public class ThermostatsListViewModel : ViewModelBase
    {
        private DelegateCommand<PlaceResource> _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
                                                 new DelegateCommand<PlaceResource>(OnItemTappedCommandExecuted));

        private DelegateCommand _refreshCommand;
        public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
                                                     new DelegateCommand(OnRefreshCommandExecuted));

        public ThermostatsListViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
        {
        }

        private ObservableCollection<PlaceResource> _thermostats;
        public ObservableCollection<PlaceResource> Thermostats
        {
            get { return _thermostats; }
            set { SetProperty(ref _thermostats, value); }
        }

        private PageState _state = PageState.NoData;
        public PageState State
        {
            get => _state;
            set => SetProperty(ref _state, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {   
            Thermostats = new ObservableCollection<PlaceResource>(new List<PlaceResource>()
            {
                new PlaceResource("Herb's office "),
                new PlaceResource("Something Thermostat")
            });
            State = PageState.Normal;
        }

        private async void OnItemTappedCommandExecuted(PlaceResource thermostat)
        {
            //TODO: ItemTap implemantation
        }

        private async void OnRefreshCommandExecuted()
        {
            IsRefreshing = true;
            //TODO: Refresh implemantation
            IsRefreshing = false;
        }
    }
}
