﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ResidentApp.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms.GoogleMaps;

namespace ResidentApp.ViewModels
{
    public class MapPageViewModel : ViewModelBase
    {
        public MapPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
        {
            
        }

        private Position _currentPosition;
        public Position CurrentPosition
        {
            get { return _currentPosition; }
            set { SetProperty(ref _currentPosition, value); }
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            CurrentPosition = new Position(10,10);//mock data
        }
    }
}
