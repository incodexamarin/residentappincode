﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Prism.AppModel;
using ResidentApp.Services.Abstractions;

namespace ResidentApp.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IApplicationLifecycleAware, IPageLifecycleAware
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value, () => RaisePropertyChanged(nameof(IsNotBusy))); }
        }

        public bool IsNotBusy
        {
            get { return !IsBusy; }
        }

        protected INavigationService _navigationService { get; }
        protected IAccessTokenApiService _accessTokenApiService { get; }
        public ViewModelBase(INavigationService navigationService, IAccessTokenApiService accessTokenApiService)
        {
            _navigationService = navigationService;
            _accessTokenApiService = accessTokenApiService;
        }

        protected async Task ValidationOAuthToken()
        {
            await _accessTokenApiService.ValidateTokenAsync();
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
        }

        public virtual void OnResume()
        {
        }

        public virtual void OnSleep()
        {
        }

        public virtual void OnAppearing()
        {
        }

        public virtual void OnDisappearing()
        {
        }
    }
}
