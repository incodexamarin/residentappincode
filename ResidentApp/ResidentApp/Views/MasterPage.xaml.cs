﻿using Prism.Navigation;
using Xamarin.Forms;

namespace ResidentApp.Views
{
    public partial class MasterPage : MasterDetailPage, IMasterDetailPageOptions
    {
        public MasterPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }
        public bool IsPresentedAfterNavigation => Device.Idiom != TargetIdiom.Phone;
    }
}