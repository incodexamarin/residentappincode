﻿using Xamarin.Forms;

namespace ResidentApp.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            //NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
        }
    }
}
