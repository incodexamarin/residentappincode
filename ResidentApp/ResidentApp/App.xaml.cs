﻿using System;
using System.Diagnostics;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using Prism;
using Prism.Ioc;
using HabSDK.Api;
using Prism.Plugin.Popups;
using ResidentApp.ViewModels;
using ResidentApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Prism.Unity;
using ResidentApp.Services;
using ResidentApp.Services.Abstractions;
using ResidentApp.Utils;
using ResidentApp.ViewModels.PopupViewModels;
using ResidentApp.Views.Popups;
using Xamarin.Forms.Essentials.Controls;
using IUsersApi = com.knetikcloud.Api.IUsersApi;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ResidentApp
{
    public partial class App : PrismApplication
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override void OnInitialized()
        {
            try
            {
                InitializeComponent();
                StateContainer.Init();
               
                NavigationService.NavigateAsync("Navigation/Login");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            Configuration.Default.BasePath = Constants.KNETIK_JSPAI_URL;
            HabSDK.Client.Configuration.Default.BasePath = Constants.HAB_API_LINK;

            containerRegistry.RegisterPopupNavigationService();

            containerRegistry.RegisterForNavigation<NavigationPage>("Navigation");
            containerRegistry.RegisterForNavigation<LoginPage>("Login");
            containerRegistry.RegisterForNavigation<MasterPage>("Index");
            containerRegistry.RegisterForNavigation<ThermostatsList>("ThermostatsList");
            containerRegistry.RegisterForNavigation<ThermostatDetailPage>("Thermostat");
            containerRegistry.RegisterForNavigation<MapPage>("Map");
            containerRegistry.RegisterForNavigation<ForgotPwdPopupPage, ForgotPwdPopupPageViewModel>("ForgotPassword");

            containerRegistry.RegisterInstance(typeof(IAccessTokenApiService), new AccessTokenApiService());
            containerRegistry.RegisterInstance(typeof(IUsersApi), new com.knetikcloud.Api.UsersApi());
            containerRegistry.RegisterInstance(typeof(IResidentsApi), new ResidentsApi());
        }
    }
}
